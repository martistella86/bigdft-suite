BigDFT\.Fragments module
========================

.. automodule:: BigDFT.Fragments
    :members:
    :undoc-members:
    :show-inheritance:

BigDFT\.Atom module
-------------------
.. automodule:: BigDFT.Atom
    :members:
    :undoc-members:
    :show-inheritance:
