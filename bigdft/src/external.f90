!> @file
!! @brief External routines of the BigDFT library.
!! @details
!! To be documented in detail once stabilized
!! All the call to BigDFT code should be performed from these routines
!! No interface should be required to manipulate these routines
!! Non-intrinsic objects should be mapped to addresses which have to be manipulated
!! @author
!!    Copyright (C) 2007-2015 BigDFT group <br>
!!    This file is distributed under the terms of the
!!    GNU General Public License, see ~/COPYING file
!!    or http://www.gnu.org/copyleft/gpl.txt .
!!    For the list of contributors, see ~/AUTHORS


subroutine bigdft_mpi_init(ierr)
  use wrapper_mpi, only: wmpi_init_thread,MPI_SUCCESS
  use module_types, only: bigdft_init_errors,bigdft_init_timing_categories
  use sparsematrix_base
  implicit none
  integer, intent(out) :: ierr

  call wmpi_init_thread(ierr)

  if (ierr == MPI_SUCCESS) then
     call bigdft_init_errors()
     call sparsematrix_init_errors()
     call bigdft_init_timing_categories()
  end if
end subroutine bigdft_mpi_init


subroutine bigdft_init_mpi_env(mpi_info,mpi_groupsize, ierr)
  use BigDFT_API
  implicit none
  
  integer, dimension(4), intent(out) :: mpi_info
  integer, intent(in) :: mpi_groupsize
  integer, intent(out) :: ierr
  !local variables
  integer :: iproc,nproc,ngroup_size

  !here wrappers for MPI should be used
  call MPI_COMM_RANK(MPI_COMM_WORLD,iproc,ierr)
  call MPI_COMM_SIZE(MPI_COMM_WORLD,nproc,ierr)
  if (ierr /= MPI_SUCCESS) return

  !set the memory limit for the allocation library
  call f_malloc_set_status(iproc=iproc)

  !if the taskgroup size is not a divisor of nproc do not create taskgroups
  if (nproc >1 .and. mpi_groupsize > 0 .and. mpi_groupsize < nproc .and.&
       mod(nproc,mpi_groupsize)==0) then
     ngroup_size=mpi_groupsize
  else
     ngroup_size=nproc
  end if
  call mpi_environment_set(bigdft_mpi,iproc,nproc,MPI_COMM_WORLD,ngroup_size)

  !final values
  mpi_info(1)=bigdft_mpi%iproc
  mpi_info(2)=bigdft_mpi%nproc
  mpi_info(3)=bigdft_mpi%igroup
  mpi_info(4)=bigdft_mpi%ngroup
end subroutine bigdft_init_mpi_env

subroutine bigdft_finalize(ierr)
  use BigDFT_API
  implicit none
  integer, intent(out) :: ierr

  ierr=0

  call bigdft_python_finalize()

  !here a routine to free the environment should be called
  call fmpi_barrier() !over comm world
  !call MPI_BARRIER(MPI_COMM_WORLD,ierr)
  call release_mpi_environment(bigdft_mpi)
  !wait all processes before finalisation
  call fmpi_barrier() !over comm world
  call mpifinalize()

end subroutine bigdft_finalize


function bigdft_error_ret(err_signal,err_message) result (ierr)
  implicit none
  character(len=*), intent(in) :: err_message
  integer, intent(in) :: err_signal
  integer :: ierr

  ierr=err_signal
  
end function bigdft_error_ret


!> Abort bigdft program
subroutine bigdft_severe_abort()
  use module_base
  use yaml_output, only: yaml_comment,yaml_flush_document
  implicit none
  integer :: ierr
  !local variables
  character(len=128) :: filename,debugdir
  !the MPI_ABORT works only in MPI_COMM_WORLD
  if (bigdft_mpi%ngroup > 1) then
     call f_strcpy(src='bigdft-err-'+bigdft_mpi%iproc+'-'+bigdft_mpi%igroup+'.yaml',&
       dest=filename)
  else
     call f_strcpy(src='bigdft-err-'+bigdft_mpi%iproc+'.yaml',&
          dest=filename)
  end if
  !create the directory debug if it does not exists
  call f_mkdir('debug',debugdir) !this in principle should not crash if multiple cores are doing it simultaneously
  call f_strcpy(dest=filename,src=debugdir+filename)
  call f_malloc_dump_status(filename=filename)
  if (bigdft_mpi%iproc ==0) then
     call f_dump_all_errors(-1)
     call yaml_comment('Error raised!',hfill='^')
     call yaml_comment('Messages are above, dumping run status in file(s) '//&
          'bigdft-err-*.yaml of directory debug/',hfill='^')
     call yaml_comment('Exiting...',hfill='~')
     call yaml_flush_document() !might help, sometimes..
  end if
  !call f_lib_finalize()
  call f_pause(1) !< wait one second
  call MPI_ABORT(MPI_COMM_WORLD,816437,ierr)
  !call mpifinalize()
  !stop
  if (ierr/=0) stop 'Problem in MPI_ABORT'

end subroutine bigdft_severe_abort
